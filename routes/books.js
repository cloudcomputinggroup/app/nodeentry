var express = require('express');
const path = require("path");
const fs = require("fs");
const YAML = require("yaml");
const https = require("https");
var router = express.Router();

const configPath = path.join(__dirname, '..', 'data', 'config.yaml')
//const testPath = path.join(__dirname, '..', 'data', 'test.json')
const file = fs.readFileSync(configPath, 'utf8')
//const jsfile = fs.readFileSync(testPath, 'utf8')
const data = YAML.parse(file)
//const jsdata = YAML.parse(jsfile)


/* GET users listing. */
router.get('/', async(req, res, next) => {

  console.log(data.books.host);
  console.log(data.books.port);
  const http = require('http')
  const options = {
    hostname: data.books.host,
    port: data.books.port,
    path: '/',
  }

  try {
    const hreq = await http.get(options, hres => {
      console.log(`statusCode: ${hres.statusCode}`);
      let data = '';
      hres.on('data', d => {
        data += d;

      })
      hres.on('end', () => {
        res.render('books', { title: 'BOOOKS', books: JSON.parse(data)});
      })
    })
  }catch (e) {
    console.log('ERROR: ', e.message);
    res.render('books', { title: 'BOOOKS', message: "Failed to load the book list"});
  }
  // data.books.
});

module.exports = router;
