var express = require('express');
var router = express.Router();

const axios = require('axios')

const fs = require('fs')
const path = require('path')
const YAML = require('yaml')

const configPath = path.join(__dirname, '..', 'data', 'config.yaml')
const file = fs.readFileSync(configPath, 'utf8')
const data = YAML.parse(file)

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('index', { title: data.app.title });
});

router.post('/', async(req, res, next) => {

  console.log(req.body.title);
  console.log(req.body.writer);

  const title = req.body.title
  const writer = req.body.writer

  //res.render('index', {title: data.app.title, message: "Message sent"} );

  console.log(data.books.host);
  console.log(data.books.port);

  const body = {title: title, writer: writer}
    axios
        .post(`http://${data.books.host}:${data.books.port}`, body)
        .then(hres => {
          console.log(`statusCode: ${hres.status}`)
          console.log(hres)
          res.render('index', {title: data.app.title, message: "Message sent"} );
        })
        .catch(error => {
          console.error(error)
          res.render('index', { title: data.app.title, message: "Data sending failed" });
        })
});

module.exports = router;

